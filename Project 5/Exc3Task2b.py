import numpy as np
from scipy import integrate


In = np.loadtxt('Input2.txt')
np.random.seed(seed=int(In))


class Model:

    def __init__(self):
      self.ka = 0.5
      self.ke = 0.3
     

      #X_0
      self.initial_state = np.array([[200],[0]])
    
      

      self.S = np.array([[-1,0],    #x0
                         [1, -1]])   #x1


    # compute reaction rates for given states in X
    def compute_rr(self, X):
      R = np.zeros((2,1))
      R[0] = X[0] * self.ka        #r1
      R[1] = X[1] * self.ke        #r2
      
      return R



def SSA(model, t_final):
  
  X_store = np.zeros(25)
  T_store = list(range(0,25))
  t = 0.0
  pp=1
  x = model.initial_state
  

  while t <= t_final:
    #print(t)
    R = model.compute_rr(x)
    u1 = np.random.uniform(0, 1)
    if(np.sum(R) == 0):
        tau = 0
    else: 
        tau = (1 / (np.sum(R))) * np.log(1 / u1)
   
    
    if ((t + tau) > t_final) or np.sum(R) == 0:
      return np.array(X_store), np.array(T_store)
    else:
      if(t<pp and t+tau >pp):
        while pp < t+tau:
         X_store[pp] = x[1] 
         pp += 1
      t += tau 
      j = 1
      u2 = np.random.uniform(0, 1)

      for i in range(len(model.S[0])):
        
        if u2 <= (np.sum(R[:i+1]) / np.sum(R)):
          j = i
          break

      
    for i in range(len(x)):
        x[i] = x[i] + model.S[i][j]


        
nsim =[4,16,64,256]

N_states=[]
N_times=[]
means_all=[]
T = [[] for _ in range(0,24) ]

for i in nsim:
    for j in range(0,i):

        model2 = Model()
        model2.R = model2.compute_rr(model2.initial_state)
        states, times = SSA(model2, 24)
        for i in range(24):
            T[i].append(states[i])


        N_states.append(states)
        N_times.append(times)

    
    mean_T=[] 
    for i in T:
        m = np.mean(i)
        mean_T.append(m)
    means_all.append(mean_T)



    
    

def func_mod(t, X):
    ka = 0.5
    ke = 0.3
    S = np.array([[-1,0],    #x0
                 [1, -1]])   #x1
    R = np.zeros((2,1))
    R[0] = X[0] * ka        #r1
    R[1] = X[1] * ke        #r2
    dx = np.dot(S, R)
    dx = np.concatenate(dx, axis=None)
    return dx


## calc ode ###


model3 = Model()
model3.R = model3.compute_rr(model3.initial_state)
solution = integrate.solve_ivp(func_mod, t_span = (0, 24), y0 = np.array([200,0]), first_step = 1., max_step = 1.0)
ode= solution.y[1]



### calc error 

errors_all=[]



def calc_err(sim, ode): 
    r=[]
    for i in range(0,24):
        
        r_tmp =np.abs(sim[i] - ode[i])
        r.append(r_tmp)
    
    sum_v = np.sum(r)
    return sum_v

    
for i in range(0,4):
    
    res = calc_err(means_all[i], ode)
    
    error = (1/25)* res
    errors_all.append(error)
    
Output = np.concatenate((np.array(nsim,ndmin=2), np.array(errors_all,ndmin=2)), axis=0)
print(f"Writing 'ErrorTask2a.txt'")
np.savetxt('ErrorTask2a' + '.txt', Output, delimiter = ',',fmt = '%1.3f')



