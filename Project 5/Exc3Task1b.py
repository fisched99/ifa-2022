import numpy as np
from scipy import integrate

initial_state = np.zeros((2,1))
initial_state = np.loadtxt('Input1.txt')

class Model:

    def __init__(self):
      self.lambd = 0.3
      self.k1    = 0.01
      self.k2    = 0.01
      self.delt2 = 0.3

      self.S = np.array([[1,-1,-1, 0],
                         [0, 0, 1,-1]])
      
      self.X = np.zeros((2,1))
      self.X = np.loadtxt('Input1.txt')
      self.R = []

    def compute_rr(self, X):
      R = np.zeros((4,1))
      R[0] = X[0] * self.lambd        #r1
      R[1] = X[0] * X[1] * self.k1         #r2
      R[2] = X[0] * X[1] * self.k2    #r3 0
      R[3] = X[1] * self.delt2        #r4 0
      return R

def dummme_scheise(t, X):
  lambd = 0.3
  k1    = 0.01
  k2    = 0.01
  delt2 = 0.3
  S = np.array([[1,-1,-1, 0], [0, 0, 1,-1]])
  R = np.zeros((4,1))
  R[0] = X[0] * lambd        #r1
  R[1] = X[0] * X[1] * k1         #r2
  R[2] = X[0] * X[1] * k2    #r3 0
  R[3] = X[1] * delt2        #r4 0
  
  dx = np.dot(S, R)
  dx = np.concatenate(dx, axis=None)
  #print(dx)
  return dx


def RHS(model):
  dx = np.dot(model.S, model.R)
  dx = np.concatenate(dx, axis=None)
  #print(dx)
  return dx

  
def euler(model, t, delta_t):
  #print(model.X)
  t += delta_t
  #dx = np.concatenate(RHS(model), axis=None)
  model.X += RHS(model) * delta_t
  return t

step_store = []
error_store = []
stpe_size = 0.010

for stpe_size in [0.010, 0.020, 0.050, 0.100]:

  model3 = Model()
  model3.R = model3.compute_rr(model3.X)
  X_store = []
  T_store = []
  t = 0
  while t <= 100:
    #print(t)
    T_store.append(t)
    X_store.append(model3.X[1])
    t = euler(model3, t, stpe_size)
    model3.R = model3.compute_rr(model3.X)

  solution = integrate.solve_ivp(dummme_scheise, t_span = (0, 100), y0 = initial_state, first_step = stpe_size, max_step = stpe_size, min_step = stpe_size)

  #print(solution.y[1])
  #print(f"stepzize: {stpe_size}, mean error: {np.sum(abs(X_store - solution.y[1][:-1])) / len(X_store)}")
  step_store.append(stpe_size)
  error_store.append(np.sum(abs(X_store - solution.y[1][:-1])) / len(X_store))
  #print(f"stepzize: {solution.y[1][:10]},\n {X_store[:10]}")

#error_store = abs(X_store - solution.y[1][:-1])
#print(error_store)



print(f"Writing 'ErrorTask1b.txt'")
Output = np.concatenate((np.array(step_store,ndmin=2), np.array(error_store,ndmin=2)), axis=0)
np.savetxt('ErrorTask1b.txt', Output, delimiter = ',',fmt = '%1.3f')


#!diff Task1aTraj.txt Task1aTrajectory.txt
#!diff ErrorTask1b.txt MyErrorTask1b.txt
