import numpy as np

In = np.loadtxt('Input2.txt')
print(In)
np.random.seed(seed=int(In))
#N = int(In[1])

class Model:

    def __init__(self):
      self.ka = 0.5
      self.ke = 0.3
     

      #X_0
      self.initial_state = np.array([[200],[0]])
    
      #self.X[0] = 200      #x0
      #self.X[1] = 0        #x1

      self.S = np.array([[-1,0],    #x0
                         [1, -1]])   #x1


    # compute reaction rates for given states in X
    def compute_rr(self, X):
      R = np.zeros((2,1))
      R[0] = X[0] * self.ka        #r1
      R[1] = X[1] * self.ke        #r2
      
      return R



    
def SSA(model, t_final):
  
  X_store = np.zeros(25)
  T_store = list(range(0,25))
  t = 0.0
  count=1
  x = model.initial_state
  

  while t <= t_final:
    #print(t)
    R = model.compute_rr(x)
    u1 = np.random.uniform(0, 1)
    if(np.sum(R) == 0):
        tau = 0
    else: 
        tau = (1 / (np.sum(R))) * np.log(1 / u1)
   
    
    if ((t + tau) > t_final) or np.sum(R) == 0:
      return np.array(X_store), np.array(T_store)
    else:
      if(t<count and t+tau >count):
        while count < t+tau:
         X_store[count] = x[1] 
         count = count + 1
      t += tau 
      j = 1
      u2 = np.random.uniform(0, 1)

      for i in range(len(model.S[0])):
        
        if u2 <= (np.sum(R[:i+1]) / np.sum(R)):
          j = i
          break

      
    for i in range(len(x)):
        x[i] = x[i] + model.S[i][j]




model1 = Model()
model1.R = model1.compute_rr(model1.initial_state)
states, times = SSA(model1, 24)

print(states)
print(times)
Output = np.concatenate((np.array(times,ndmin=2), np.array(states,ndmin=2)), axis=0)
print(f"Writing 'Task2aTraj.txt'")
np.savetxt('Task2aTraj' + '.txt', Output, delimiter = ',',fmt = '%1.2f')


