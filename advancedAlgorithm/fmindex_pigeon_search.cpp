#include <sstream>

#include <seqan3/alphabet/nucleotide/dna5.hpp>
#include <seqan3/argument_parser/all.hpp>
#include <seqan3/core/debug_stream.hpp>
#include <seqan3/io/sequence_file/all.hpp>
#include <seqan3/search/fm_index/fm_index.hpp>
#include <seqan3/search/search.hpp>


bool greaterEqual(auto const& query, unsigned long suffix_start_index, std::vector<seqan3::dna5> const& reference, unsigned k ){

    unsigned dist = 0;

	for(unsigned i = 0; i < query.size(); i++){
        
        if( dist > k ){
            
            return false;
        }
		
		// query is smaller than reference
		if( query[i] < reference[suffix_start_index + i] ){
		
			dist++;
		}
		// query is bigger than reference
		else if( query[i] > reference[suffix_start_index + i] ){
		
			return true;
		}
		// query is equal reference
		else{
			// reference end reached query can not be smaller anymore now
			if( i + suffix_start_index == reference.size() -1 ){
				return true;
			}
		}
	}
	// query end is reached and query was not smaller than ref
	return true;
    
}

bool smallerEqual(auto const& query, unsigned long suffix_start_index, std::vector<seqan3::dna5> const& reference, unsigned k){

    unsigned dist = 0;

	for(unsigned i = 0; i < query.size(); i++){
        
        if( dist > k ){
            
            return false;
        }

		// query is smaller than reference
		if( query[i] < reference[i + suffix_start_index] ){
		
			return true;
		}
		// query is bigger than reference
		else if( query[i] > reference[i + suffix_start_index] ){
		
			dist++;
		}
		// query is equal reference
		else{
			// reference end reached but query end is not reached yet
			if( i + suffix_start_index == reference.size() - 1){

				return i != query.size() - 1;
			}
		}
	}
	// query end is reached query and was not bigger than ref
	return true;
    
}

int main(int argc, char const* const* argv) {
    seqan3::argument_parser parser{"fmindex_pigeon_search", argc, argv, seqan3::update_notifications::off};

    parser.info.author = "SeqAn-Team";
    parser.info.version = "1.0.0";

    auto index_path = std::filesystem::path{};
    parser.add_option(index_path, '\0', "index", "path to the query file");

    auto query_file = std::filesystem::path{};
    parser.add_option(query_file, '\0', "query", "path to the query file");

    try {
         parser.parse();
    } catch (seqan3::argument_parser_error const& ext) {
        seqan3::debug_stream << "Parsing error. " << ext.what() << "\n";
        return EXIT_FAILURE;
    }

    // loading our files
    auto query_stream     = seqan3::sequence_file_input{query_file};

    // read query into memory
    std::vector<std::vector<seqan3::dna5>> queries;
    for (auto& record : query_stream) {
        queries.push_back(record.sequence());
    }

    // loading fm-index into memory
    using Index = decltype(seqan3::fm_index{std::vector<std::vector<seqan3::dna5>>{}}); // Some hack
    Index index; // construct fm-index
    {
        seqan3::debug_stream << "Loading 2FM-Index ... " << std::flush;
        std::ifstream is{index_path, std::ios::binary};
        cereal::BinaryInputArchive iarchive{is};
        iarchive(index);
        seqan3::debug_stream << "done\n";
    }

    // read reference into memory
    // Attention: we are concatenating all sequences into one big combined sequence
    //            this is done to simplify the implementation of suffix_arrays
    auto reference_stream = seqan3::sequence_file_input{"../data/hg38_partial.fasta.gz"};
    seqan3::debug_stream << "Loading reference file.. " << std::flush;
    std::vector<seqan3::dna5> reference;
    for (auto& record : reference_stream) {
        auto r = record.sequence();
        reference.insert(reference.end(), r.begin(), r.end());
    }

    unsigned k = 3;

    seqan3::configuration const cfg = seqan3::search_cfg::max_error_total{seqan3::search_cfg::error_count{0}};

    //!TODO here adjust the number of searches
    queries.resize(100); // will reduce the amount of searches


    //!TODO !ImplementMe use the seqan3::search to find a partial error free hit, verify the rest inside the text
    //Pseudo code (might be wrong):
    //for query in queries:
    //     parts[3] = cut_query(3, query);
    //     for p in {0, 1, 2}:
    //         for (pos in search(index, part[p]):
    //             if (verify(ref, query, pos +- ....something)):
    //                 std::cout << "found something\n"
    std::cout << "Starting FM-Index search with " << k << "errors ..\n";

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();


    for (auto& q : queries) {

        std::vector<size_t> result;
        unsigned hits_size = 0;
        
        // calculate first sub query
        unsigned begin = 0;
        unsigned end = q.size() / k+1;

        // iterate through all sub queries
        while (end < q.size()){

            auto subquery = seqan3::views::slice(q, begin, end);
            auto hits = seqan3::search(subquery, index);

            for (auto && hit : hits){
                hits_size++;
                size_t start = hit.reference_begin_position() ? hit.reference_begin_position() -1 : 0;

                // use old smaller / greater Equal functions to verify query with hamming dist <= k
                if(start != 0 && smallerEqual(q, start, reference, k) &&  greaterEqual(subquery, start, reference, k)){
                    result.push_back(start);
                }

            }
            std::cout << "Found " << hits_size << " hits in total, " << result.size() << " could be verified." << std::endl;

            begin = end + 1;
            end += q.size() / k+1;
        }    

    }
    
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Time elapsed during search: " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[μs]" << std::endl;
    std::cout << "Done!\nWhy was the negative-sense DNA angry at the positive-sense DNA? Because he was left stranded!" << std::endl;


    return 0;
}
