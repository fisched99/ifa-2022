#include <divsufsort.h>
#include <sstream>

#include <seqan3/std/filesystem>

#include <seqan3/alphabet/nucleotide/dna5.hpp>
#include <seqan3/argument_parser/all.hpp>
#include <seqan3/core/debug_stream.hpp>
#include <seqan3/io/sequence_file/all.hpp>
#include <seqan3/search/fm_index/fm_index.hpp>
#include <seqan3/search/search.hpp>
#include <chrono>

bool greaterEqual(std::vector<seqan3::dna5> const& query, unsigned long suffix_start_index, std::vector<seqan3::dna5> const& reference ){

	for(unsigned i = 0; i < query.size(); i++){
		
		// query is smaller than reference
		if( query[i] < reference[suffix_start_index + i] ){
		
			return false;
		}
		// query is bigger than reference
		else if( query[i] > reference[suffix_start_index + i] ){
		
			return true;
		}
		// query is equal reference
		else{
			// reference end reached query can not be smaller anymore now
			if( i + suffix_start_index == reference.size() -1 ){
				return true;
			}
		}
	}
	// query end is reached
	return true;
    
}

bool smallerEqual(std::vector<seqan3::dna5> const& query, unsigned long suffix_start_index, std::vector<seqan3::dna5> const& reference ){

	for(unsigned i = 0; i < query.size(); i++){
		
		// query is smaller than reference
		if( query[i] < reference[i + suffix_start_index] ){
		
			return true;
		}
		// query is bigger than reference
		else if( query[i] > reference[i + suffix_start_index] ){
		
			return false;
		}
		// query is equal reference
		else{
			// reference end reached but query end is not reached yet
			if( i + suffix_start_index == reference.size() - 1 && i != query.size() - 1 ){
				return false;
			}
		}
	}
	// query end is reached
	return true;
    
}


long findLowerBoundInSA ( std::vector<seqan3::dna5> const& q, std::vector<seqan3::dna5> const& reference, int *SA ){

        // lower_bound equals L, upper_bound equals R, mid equal M and q equals p from the lecture, 
        unsigned long lower_bound = 0, upper_bound = 0, mid = 0;

        if ( smallerEqual(q, SA[0], reference) ) { 

            return -1; 
        } 
        else if ( ! smallerEqual(q, SA[reference.size()-1], reference) ) {

            return -1; 
        }
        else {

            lower_bound = 0;
            upper_bound = reference.size()-1;

            while ( upper_bound - lower_bound > 1 ) {

                mid = std::ceil( (upper_bound + lower_bound) / 2);

                // if q is smaller or equal to mid, lower upper bound to mid
                if ( smallerEqual(q, SA[mid], reference) ) {

                    upper_bound = mid;
                }
                // if q is bigger than mid, rais lower bound to mid
                else {

                    lower_bound = mid;
                }

            }

            return upper_bound;
        }
}

long findUpperBoundInSA ( std::vector<seqan3::dna5> const& q, std::vector<seqan3::dna5> const& reference, int *SA, long Lp){

        // lower_bound equals L, upper_bound equals R, mid equal M and q equals p from the lecture, 
        unsigned long lower_bound = 0, upper_bound = 0, mid = 0;

        // if q is not bigger (smaller) than T[SA[lower_bound - 1]] q is not in the reference, if its equal q is T[SA[lower_bound]]
        if ( ! greaterEqual(q, SA[Lp - 1], reference) ) { 
            //std::cout << "q is smaller than lower bound\n";

            return -1; 
        } 
        // if q is bigger than T[SA[SA.size()]] than q can not be in the reference
        else if ( greaterEqual(q, SA[reference.size()-1], reference) ) {
            //std::cout << "q is bigger than upper bound\n";

            return -1; 
        }
        else {

            lower_bound = 0;
            upper_bound = reference.size()-1;

            while ( upper_bound - lower_bound > 1 ) {

                mid = std::ceil( (upper_bound + lower_bound) / 2);
                
                // if q is bigger or equal mid, raise lowerb bound to mid
                if ( greaterEqual(q, SA[mid], reference) ) {

                    lower_bound = mid;
                }
                // if q is smaller than mid lower upper bound to mid
                else {

                    upper_bound = mid;
                }

            }
            return lower_bound;
        }
}


int main(int argc, char const* const* argv) {
    seqan3::argument_parser parser{"suffixarray_search", argc, argv, seqan3::update_notifications::off};

    parser.info.author = "SeqAn-Team";
    parser.info.version = "1.0.0";

    auto reference_file = std::filesystem::path{};
    parser.add_option(reference_file, '\0', "reference", "path to the reference file");

    auto query_file = std::filesystem::path{};
    parser.add_option(query_file, '\0', "query", "path to the query file");

    try {
         parser.parse();
    } catch (seqan3::argument_parser_error const& ext) {
        seqan3::debug_stream << "Parsing error. " << ext.what() << "\n";
        return EXIT_FAILURE;
    }

    // loading our files
    std::cout << "Loading input files ..\n";
    auto reference_stream = seqan3::sequence_file_input{reference_file};
    auto query_stream     = seqan3::sequence_file_input{query_file};

    // read reference into memory
    // Attention: we are concatenating all sequences into one big combined sequence
    //            this is done to simplify the implementation of suffix_arrays
    std::vector<seqan3::dna5> reference;
    for (auto& record : reference_stream) {
        auto r = record.sequence();
        reference.insert(reference.end(), r.begin(), r.end());
    }

    // read query into memory
    std::vector<std::vector<seqan3::dna5>> queries;
    for (auto& record : query_stream) {
        queries.push_back(record.sequence());
    }

    //!TODO here adjust the number of searches
    queries.resize(100); // will reduce the amount of searches

    // Array that should hold the future suffix array
    std::vector<saidx_t> suffixarray;
    suffixarray.resize(reference.size()); // resizing the array, so it can hold the complete SA

    //!TODO !ImplementMe implement suffix array sort

    std::cout << "Building suffix array ..\n";

    // cast reference string 
    sauchar_t const* str = reinterpret_cast<sauchar_t const*>(reference.data());

    // allocate SA memor
    int *SA = (int *)malloc(reference.size() * sizeof(int));

    // sort SA
    divsufsort((unsigned char *)str, SA, reference.size());

    std::cout << "Starting search ..\n";


    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    for (auto& q : queries) {
        //!TODO !ImplementMe apply binary search and find q  in reference using binary search on `suffixarray`

        std::string pos_str = "";

        long Lp = findLowerBoundInSA(q, reference, SA);
        long Rp = 0;
        if ( Lp >=0 ) { 
            Rp = findUpperBoundInSA(q, reference, SA, Lp); 
        }
        else{
            Rp = -1;
        }
        if ( Lp >= 0 && Rp >= 0 && ( Lp - Rp < 0) ){
            

            for (long i = Lp; i <= Rp; i++){
                pos_str += std::to_string(SA[i]) + ", ";
            }

            if(pos_str.length() >= 2){
                pos_str.pop_back();
                pos_str.pop_back();
            }

            seqan3::debug_stream << "Found query '" << q << "' " << Rp - Lp + 1 << " time(s) in reference at position(s):\n" << pos_str << std::endl; 

        }
        else {

            seqan3::debug_stream << "Could not find query '" << q << "' in reference sequence.\n";
            
        }
    }

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    std::cout << "Time elapsed during search: " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << "[s]" << std::endl;
    std::cout << "Done!\nWhat did one bioinformatician say to another after a get together? 'Dude, that party was a BLAST!'" << std::endl;

    return 0;
}
