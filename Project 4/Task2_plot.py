
import numpy as np

#In = np.loadtxt('Input-3.txt')
#np.random.seed(seed=int(In[0]))
#N = int(In[1])

class Model:

    def __init__(self):
      self.k = [0.15, 0.0015, 20, 3.5]

      #X_0
      self.initial_state = np.zeros((1,1))
      self.initial_state[0] = 40      #x1

      self.S = np.array([[1,-1, 1,-1]])   #x1


    # compute reaction rates for given states in X
    def compute_rr(self, X):
      R = np.zeros((6,1))
      R[0] = self.k[0] * X[0] * (X[0] - 1)              #r1
      R[1] = self.k[1] * X[0] * (X[0] - 1) * (X[0] - 2)      
      R[2] = self.k[2]  #r3 0
      R[3] = self.k[3] * X[0]   #r4 0
      return R


def SSA(model, t_final):
  first = True
  X_store = []
  T_store = []
  t = 0.0
  x = model.initial_state
  X_store.append(x[0,0])
  T_store.append(t)

  while t <= t_final:
    #print(t)
    R = model.compute_rr(x)
    u1 = np.random.uniform(0, 1)
    tau = (1 / (np.sum(R))) * np.log(1 / u1)
    if first: 
      first = False
    
    if (t + tau) > t_final or np.sum(R) == 0:
      return np.array(X_store), np.array(T_store)
    else:
      t += tau 
      j = 0
      u2 = np.random.uniform(0, 1)

      for i in range(len(model.S[0])):
        j = i
        if u2 <= (np.sum(R[:i+1]) / np.sum(R)):
          break

      x += model.S[:, [j]]
    X_store.append(x[0,0])
    T_store.append(t) 


n=200
o = []
model2= Model()
for i in range(0,n):
  model2 = Model()
  states, times = SSA(model2, 5)
  
  o.append(states)
o = np.concatenate(o, axis=None)
  #print('states',states)
  #print('times',times)
  #Output = np.concatenate((np.array(times,ndmin=2), np.array(states,ndmin=2)), axis=0)
  #print(f"Writing 'Task2Traj{str(i+1)}.txt'")
  #np.savetxt('Task2Traj' + str(i+1) + '.txt', Output, delimiter = ',',fmt = '%1.3f')



    
import matplotlib.pyplot as plt 

plt.hist(o, bins='auto' , color='b' )
plt.axvline(np.mean(o), color ='r', linestyle='dashed') 
plt.title(label="Slögl model with N = 200 realisations  at T = 5  of species x1 ")
plt.style.use('ggplot')
plt.savefig('histogram_soegle.png')

