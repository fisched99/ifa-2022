import numpy as np

In = np.loadtxt('Input.txt')
np.random.seed(seed=int(In[0]))
N = int(In[1])

class Model:

    def __init__(self):
      self.lambd = 1e-4
      self.delt  = 1e-8
      self.beta  = 5e-5
      self.k_r   = 0.3

      #X_0
      self.initial_state = np.zeros((4,1))
      self.initial_state[0] = self.lambd / self.delt      #x1
      self.initial_state[1] = 20                #x2
      self.initial_state[2] = 0                 #x3
      self.initial_state[3] = 0                 #x4

      self.S = np.array([[1,-1,-1, 0, 0, 0],    #x1
                         [0, 0, 1,-1,-1, 0],    #x2
                         [0, 0, 0, 0, 1,-1],    #x3
                         [0, 0, 0, 1, 0, 0]])   #x4


    # compute reaction rates for given states in X
    def compute_rr(self, X):
      R = np.zeros((6,1))
      R[0] = self.lambd               #r1
      R[1] = X[0] * self.delt         #r2
      R[2] = X[0] * X[1] * self.beta  #r3 0
      R[3] = X[1] * 3e7 * self.delt   #r4 0
      R[4] = X[1] * self.k_r          #r5 0
      R[5] = X[2] * self.delt         #r6 0
      return R


def SSA(model, t_final):
  first = True
  X_store = []
  T_store = []
  t = 0.0
  x = model.initial_state
  X_store.append(x[1,0])
  T_store.append(t)

  while t <= t_final:
    R = model.compute_rr(x)
    u1 = np.random.uniform(0, 1)
    tau = (1 / (np.sum(R))) * np.log(1 / u1)
    if first: 
      first = False
    
    if (t + tau) > t_final or np.sum(R) == 0:
      return np.array(X_store), np.array(T_store)
    else:
      t += tau 
      j = 0
      u2 = np.random.uniform(0, 1)

      for i in range(len(model.S[1])):
        j = i
        if u2 <= (np.sum(R[:i+1]) / np.sum(R)):
          break

      x += model.S[:, [j]]
    X_store.append(x[1,0])
    T_store.append(t)


for i in range(0,N):
  model1 = Model()
  states, times = SSA(model1, 10)
  Output = np.concatenate((np.array(times,ndmin=2), np.array(states,ndmin=2)), axis=0)
  print(f"Writing 'Task1Traj{str(i+1)}.txt'")
  np.savetxt('Task1Traj' + str(i+1) + '.txt', Output, delimiter = ',',fmt = '%1.3f')
